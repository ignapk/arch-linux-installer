### Arch Linux GUI Installer: Calamares Installer Framework
---------


### Dependencies

Main:
* Compiler with C++14 support: GCC >= 5 or Clang >= 3.5.1
* CMake >= 3.3
* Qt >= 5.9
* yaml-cpp >= 0.5.1
* Python >= 3.3 (required for some modules)
* Boost.Python >= 1.55.0 (required for some modules)
* KDE extra-cmake-modules >= 5.18 (recommended; required for some modules;
  required for some tests)
* KDE Frameworks KCoreAddons (>= 5.58 recommended)
* PythonQt (optional, deprecated)

Modules:
* Individual modules may have their own requirements;
  these are listed in CMake output. Particular requirements (not complete):
* *fsresizer* KPMCore >= 3.3
* *partition* KPMCore >= 3.3
* *users* LibPWQuality (optional)

### Building

See [wiki](https://github.com/calamares/calamares/wiki) for up to date
[building](https://github.com/calamares/calamares/wiki/Develop-Guide)
and [deployment](https://github.com/calamares/calamares/wiki/Deploy-Guide)
instructions.

cd arch-linux-installer

mkdir -p build

cd build

cmake .. \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_INSTALL_LIBDIR=lib \
    -DWITH_PYTHONQT:BOOL=ON \
    -DBoost_NO_BOOST_CMAKE=ON \
    -DSKIP_MODULES="tracking webview interactiveterminal initramfs \
                    initramfscfg dracut dracutlukscfg \
                    dummyprocess dummypython dummycpp \
                    dummypythonqt services-openrc"
                    
make

make install 

The installer also requires endeavoros install script in /usr/bin: https://github.com/endeavouros-team/install-scripts/blob/master/pacstrap_calamares

And copying src/modules/netinstall/netinstall.yaml to /usr/share/calamares